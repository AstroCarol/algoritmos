#include <stdio.h>
 
int main(int argc, char *argv[]) 
{
    int n, m;
    scanf("%d %d", &n, &m);
    int matriz[m][n], i, j, principal=0, secundaria=0, lin, col;
    for (i = 0; i < m; i++) 
    {
      for (j = 0; j < n; j++) 
      {
        scanf("%d", &matriz[i][j]);
      }
    }
    for (i = 0; i < m; i++) 
    {
        for (j = 0; j < n; j++) 
        {
            if (i == j) 
            {
                principal = principal + matriz[i][j];
            }
        }
    }
    for (i = 0; i < m; i++) 
    {
        for (j = 0; j <  n; j++) 
        {
            if (i + 1 == n - (j + 1) + 1) 
            {
               secundaria += matriz[i][j];
            }
        }
    }
    for (i = 0; i < m; i++) 
    {
        lin = 0;
        for (j = 0; j <  n; j++)
        {
            lin = lin + matriz[i][j];
        }
    }
    for(i = 0; i < n; i++)
    {
        col = 0;
        for(j = 0; j < m; j++)
        {
            col = col+matriz[i][j];
        }
    }
    if(principal == secundaria && principal == lin && principal == col)
    {
        printf("Matriz quadrada, baby!\n");
    }
    else
    { 
        printf("Sorry, but nao eh uma matriz quadrada\n");
    }
    return 0;
}