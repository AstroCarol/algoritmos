#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
	int option, i, comp1, comp, v, c;
	char s1[20], s2[20], aux[20], c1, c2;
	printf("1 - Ler String\n2 - Imprimir tamanho da String\n3 - Comparar duas Strings\n4 - Concatenar duas Strings\n5 - Imprimir String de forma reversa\n6 - Contar quantas vezes um caractere aparece na String\n");
	for (int i = 1; i != 0 ; ++i)
	{
		printf("Escola uma operação:\n");
		scanf("%d", &option);
		while(option <= 0 || option > 7)
		{
			printf("Opção inválida, digite outro valor baby\n");
			scanf("%d, &option");
		}
	}
	switch(option)
	{
		case 1:
			printf("Digite uma string\n");
			scanf("%s", s1);
			printf("%s\n", s1);
			break;
		case 2:
			scanf("%s", s1);
			comp = strlen(s1);
			printf("Tamanho: %d\n", comp);
			break;
		case 3:
			scanf("%s", s1);
			scanf("%s", s2);
			printf("Digite uma nova String\n");
			scanf("%s", s2);
			comp = strlen(s1);
			comp1 = strlen(s2);
			if (comp < comp1)
			{
				printf("A String 2 é maior que a String 1\n");
			}
			else if (comp1 < comp)
			{
				printf("A String 1 é maior que a String 2\n");
			}
			else
			{
				printf("As Strings tem o mesmo tamanho\n");
			}
			break;
		case 4:
			scanf("%s", s1);
			scanf("%s", s2);
			strcat(s1, s2);
			printf("Mensagem concatenada: %s\n", s1);
			break;
		case 5:
			scanf("%s", s1);
			comp = strlen(s1);
			for (i = 0, j = comp -1; i < j; i++, j--)
			{
				c = s1[i];
				s1[i] = s1[j];
				s1[j] = c;
			}
			printf("String invertida: %s\n", s1);
			break;
		case 6:
			scanf("%s", s1);
			scanf("%c", c1);
			for (i = 0; i < strlen(s1); i++)
			{
				if (s1[i] == c1)
				{
					v++;
				}
			}
			printf("Vezes que o caracter aparece na string: %d\n", v);
			break;
	}
	return 0;
}