#include <stdio.h>

int main(int argc, char const *argv[])
{
	int n, i, p=0, j, soma=0;
	char gabarito[5], resposta[5];
	scanf("%d", &n);

	for (i = 0; i < 5; i++)
	{
		scanf(" %c", &gabarito[i]);
	}
	for (j = 1; j <= n ; j++)
	{
	  p = 0;
		for(i = 0; i < 5; i++)
		{
			scanf(" %c", &resposta[i]);
			if (resposta[i] == gabarito[i])
			{
				p++;
			}
		}
		soma += p;
		printf("O aluno %d fez %d\n", j,p);
	}
	printf("Media: %d", soma/n);
	return 0;
}
