#include <stdio.h>
 
int main(int argc, char *argv[]) 
{
    int n, m;
    scanf("%d %d", &m, &n);
    int matriz[m][n], i, j, count, lin = 0, col = 0;
    for (i = 0; i < m; i++) 
    {
      for (j = 0; j < n; j++) 
      {
        scanf("%d", &matriz[i][j]);
      }
    }
 
    for (i = 0; i < m; i++) 
    {
        count = 0;
        for (j = 0; j < n; j++) 
        {
            if (matriz[i][j] == 0) 
            {
            	count++;
            }
        }
        if (count == n) 
        {
        	lin++;
        }
    }
    
    for (i = 0; i < n; i++) 
    {
        count = 0;
        for (j = 0; j <  m; j++) 
        {
            if (matriz[j][i] == 0) 
            {
            	count++;
            }
        }
        if (count == m) 
        {
        	col++;
        }
    }
    printf("Tem %d linha(s) e %d coluna(s) nula(s)", lin, col);
 
    return 0;
}