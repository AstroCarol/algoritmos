/*Uma Empresa de fornecimento de energia elétrica faz a leitura mensal dos
medidores de consumo. Para cada consumidor, são digitados os seguintes dados:
- Número do consumidor;
- Quantidade de kWh consumidos durante o mês;
- Tipo (código) do consumidor.
1 – residencial, preço em reais por kWh = 0,3
2 – comercial, preço em reais por kWh = 0,5
3 – industrial, preço em reais por kWh = 0,7
Os dados devem ser lidos até que seja encontrado um consumidor com Número 0
(zero). Escreva um programa que calcule e imprima:
- O custo total para cada consumidor;
- O total de consumo para os três tipos de consumidor;
- A média de consumo dos tipos 1 e 2.*/

#include <stdio.h>
int main(int argc, char *argv[])
{
    int numConsumidor = 1, tipo;
    float kWh, consumo, tConsumo, con1, con2, con3, media;
    
    while(numConsumidor!=0)
    {
        printf("Número do consumidor: \n");
        scanf("%d",&numConsumidor);
        if(numConsumidor!=0)
        {
            printf("Quantidade de kWh consumidos durante o mês;\n");
            scanf("%f",&kWh);
            printf("Código do consumidor\n 1 – residencial\n 2 – comercial\n 3 – industrial\n");
            scanf("%d",&tipo);
            if(tipo == 1)
            {
                consumo = kWh * 0.3;
                con1 = consumo + con1;
            }
            else if(tipo == 2)
            {
                consumo = kWh * 0.5;
                con2 = consumo + con2;
            }
            else if(tipo == 3)
            {
                consumo = kWh * 0.7;
                con3 = consumo + con3;
            }
        }
    }
    tConsumo = con3 + con2 + con1;
    media = con1 + con2 / 2;
    printf("Consumo geral de cada consumidor: %.2f\n", consumo);
    printf("Consumo total %.2f\n", tConsumo);
    printf("Media dos tipos 1 e 2: %.2f\n", media);
    return 0;
}