/*A série de RICCI difere da série de FIBONACCI porque os dois primeiros termos são
fornecidos pelo usuário. Os demais termos são gerados da mesma forma que a série
de FIBONACCI. Criar um programa que imprima os N primeiros termos da série de
RICCI e a soma dos termos impressos, sabendo-se que para existir esta série serão
necessários pelo menos três termos.*/

#include <stdio.h>

int main(int argc, char *argv[])
{
	int t1, t2=0, count, n, soma, i;
	printf("Digite o número de termos da sequência a serem exibidos:\n");
  	scanf("%d",&n);
  	printf("Digite o primeiro termo da série:\n");
  	scanf("%d", &t1);
  	printf("Digite o segundo termo da série:\n");
  	scanf("%d", &t2);
  	for(i = 0; i < n; i++)
  	{
    	count = t1 + t2;
    	t1 = t2;
    	t2 = count;
		soma = soma + count;
    	printf("%d\n", count);
	}
	printf("A soma dos termos é: %d\n", soma);
	return 0;
}