/*Anacleto tem 1,50 m e cresce 2 cm por ano, enquanto Felisberto tem 1,10 m e cresce
3 cm por ano. Construa um programa que calcule e imprima quantos anos serão
necessários para que Felisberto seja maior que Anacleto.*/

#include <stdio.h>

int main()
{
	int aux = 0;
	float anacleto, felisberto;
	anacleto = 1.50;
	felisberto = 1.10;
	while(felisberto <= anacleto)
	{
		felisberto = felisberto + 0.03;
		anacleto = anacleto + 0.02;
		aux++;	
	}
	printf("Serão necessários %d anos para que Felisberto seja maior que Anacleto", aux);
	return 0;
}