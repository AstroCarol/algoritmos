/*Sabe-se que a cultura de uma certa bactéria dobra seu volume a cada dia. Dados um
número de dias n (inteiro) e um volume v (ponto flutuante), qual deve ser o volume
inicial para que em n dias se obtenha, pelo menos, um volume v desta cultura?*/

#include <stdio.h>

int main(int argc, char *argv[])
{
	int n;
	float v;
	printf("Quantidade de dias:\n");
	scanf("%d", &n);
	printf("Digite o volume:\n");
	scanf("%f", &v);
	while(n > 0)
	{
		v /= 2;
		n--;
	}
	printf("Volume inicial: %.2f\n", v);
	return 0;
}