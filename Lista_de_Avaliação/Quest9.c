/*Escreva um programa que receba vários números e verifique se eles são ou não
quadrados perfeitos. O programa termina a execução quando for digitado um
número menor ou igual a 0.
(Um número é quadrado perfeito quando tem um número inteiro como raiz
quadrada.)*/

#include <stdio.h>

int quaPerfeito(int num)
{
	int aux = 1;
	int i;
	for (i = 0; aux != 0; ++i)
	{
		if (aux * aux == num)
		{
			return num;
			break;
		}
		aux++;
	}
}

int main(int argc, char const *argv[])
{
	int aux = 1, num, fun;
	while(aux > 0)
	{
		scanf("%d", &num);
		fun = quaPerfeito(num);
		printf("%d é um quadrado perfeito\n", fun);
	}
	return 0;
}