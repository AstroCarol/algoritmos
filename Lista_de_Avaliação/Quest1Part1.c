/*Faça um programa que Imprima uma tabela de conversão de polegadas para
centímetros, de 1 a 20. Considere que Polegada = Centímetro * 2,54.*/

#include <stdio.h>
#define CON 2.54

int main(int argc, char *argv[])
{
	float p, c = 1;
	while(c < 21)
	{
		p = c * CON;
		printf("|Centíemtros %.2f| |Polegadas %.2f|\n", c, p);
		c++;
	}
	return 0;
}