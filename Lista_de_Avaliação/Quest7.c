/*Em um prédio, com 50 moradores, há três elevadores denominados A, B e C. Para
otimizar o sistema de controle dos elevadores, foi realizado um levantamento no
qual cada usuário respondia:
- o elevador que utilizava com mais freqüência;

- o período que utilizava o elevador, entre:
- 1 = matutino;
- 2 = vespertino;
- 3 = noturno.
Construa um programa que calcule e imprima:
- qual é o elevador mais freqüentado e em que período se concentra o maior fluxo;
- qual o período mais usado de todos e a que elevador pertence;
- qual a diferença percentual entre o mais usado dos horários e o menos usado;
- qual a percentagem sobre o total de serviços prestados do elevador de média
utilização.*/

#include <stdio.h>

int main(int argc, char const *argv[])
{
	int turno, i = 0, countA = 0, countB = 0, countC = 0;
	int manha = 0, tarde = 0, noite = 0;
	char elevador;
	while (i < 6)
	{
		printf("Qual elevador voce prefere? A, B ou C? \n");
		scanf(" %c", &elevador);
		printf("Qual o turno? 1, 2 ou 3?\n");
		scanf("%d", &turno);
		if (elevador == 'A' || elevador == 'a')
		{
			countA++;
		}
		else if (elevador == 'B' || elevador == 'b')
		{
			countB++;
		}
		else if (elevador == 'C' || elevador == 'c')
		{
			countC++;
		}
		if (turno == 1)
		{
			manha++;
		}
		else if (turno == 2)
		{
			tarde++;
		}
		else if (turno == 3)
		{
			noite++;
		}
		i++;
	}
	
	if (countA > countB && countA > countC)
	{
		printf("Elevador A eh o mais frequente com %d vezes\n", countA);
	}
	else if (countB > countA && countB > countC)
	{
		printf("Elevador B eh o mais frequente com %d vezes\n", countB);
	}
	else if (countC > countA && countC > countB)
	{
		printf("Elevador C eh o mais frequente com %d vezes\n", countC);
	}
	
	if (manha > tarde && manha > noite)
	{
		printf("Turno mais frequente eh manha\n");
	}
	else if (manha < tarde && tarde > noite)
	{
		printf("Turno mais frequente eh tarde\n");
	}
	else if (manha < noite && tarde < noite)
	{
		printf("Turno mais frequente eh noite\n");
	}
	return 0;
}

