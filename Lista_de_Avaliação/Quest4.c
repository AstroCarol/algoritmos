/*Escrever um programa para calcular a soma dos pesos das pessoas com mais de
trinta anos. O usuário deverá informar a quantidade de pessoas e a idade e o peso
de cada pessoa.*/

#include <stdio.h>

int main()
{
	int id, aux;
	float peso, total=0;
	printf("Digite a quantidade de pessoas:");
	scanf("%d", &aux);
	while(aux > 0)
	{
		scanf("%d", &id);
		scanf("%f", &peso);
		if(id >= 30)
		{		
			total = peso + total;
		}
		aux--;	
	}
	printf("Total de pesos: %.2f", total);
	return 0;
}