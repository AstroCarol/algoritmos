/*Criar um programa que calcule o M.M.C (mínimo múltiplo comum) entre dois
números lidos. (por exemplo: o M.M.C, entre 10 e 15 é 30).*/

#include <stdio.h>

int mmc(num1, num2)
{
	int count, i, mmc;
	for (i = 2; i <= num2 ; ++i)
	{
		count = num1 * i;
		if (count % num2 == 0)
		{
			mmc = count;
			i = num2 + 1;
		}
	}
	return mmc;
}

int main(int argc, char const *argv[])
{
	int num1, num2, result;
	printf("Digite os numeros que quer o mmc\n");
	scanf("%d %d", &num1, &num2);
	result = mmc(num1, num2);
	printf("MMC de %d e %d: %d\n", num1, num2, result);
	return 0;
}