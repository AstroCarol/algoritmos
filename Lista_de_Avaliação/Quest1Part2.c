/*Em uma eleição presidencial existem quatro candidatos. Os votos são informados
por código.
Os dados utilizados obedecem à seguinte codificação:
* 1, 2, 3, 4 = voto para os respectivos candidatos;
* 5 = voto nulo;
* 6 = voto em branco.
Elabore um programa que calcule e escreva:
* o total de votos para cada candidato e seu percentual sobre o total;
* o total de votos nulos e seu percentual sobre o total;
* o total de votos em branco e seu percentual sobre o total.
Como finalizador do conjunto de votos, tem-se o valor 0 (zero).*/

#include <stdio.h>

int main(int argc, char *argv[]) 
{
  int c1 = 0, c2 = 0, c3 = 0, c4 = 0, nulo = 0, branco = 0, cod, vot;
	float total;
	printf("Digite a quantidade de votos a ser recebida: \n");
	scanf("%d", &vot);  
	while(vot > 0) {
    	printf("Digite o numero do candidato que deseja votar: \n");
    	scanf("%d", &cod);
    	if (cod == 1) 
		{
 	     	c1++;
		} 
		else if (cod == 2) 
		{
      		c2++;
    	} 
		else if (cod == 3) 
		{
	      	c3++;
	    } 
		else if (cod == 4) 
		{
      		c4++;
		} 
		else if (cod == 5) 
		{
      		nulo++;
		} 
		else if (cod == 6) 
		{
      		branco++;
		}
    	vot--;
  	}

total = c1 + c2 + c3 + c4 + nulo + branco;

printf("Candidato 1: %d Voto(s) (Percentual: %.2f%)\n", c1, c1*100/total);
printf("Candidato 2: %d Voto(s) (Percentual: %.2f%)\n", c2, c2*100/total);
printf("Candidato 3: %d Voto(s) (Percentual: %.2f%)\n", c3, c3*100/total);
printf("Candidato 4: %d Voto(s) (Percentual: %.2f%)\n", c4, c4*100/total);
printf("%d Voto(s) nulos (Percentual: %.2f%)\n", nulo, nulo*100/total);
printf("%d Voto(s) brancos (Percentual: %.2f%)\n", branco, branco*100/total);

return 0;
}