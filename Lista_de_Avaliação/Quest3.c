/*Escrever um programa para determinar quantas pessoas acima de 18 anos tem uma
estatura superior a 1,60 metros. O usuário deverá informar a idade e o peso de cada
pessoa. O programa deve terminar quando o usuário informar um valor negativo
para a idade*/

#include <stdio.h>

int main()
{
	int id, count;
	float alt;
	while(scanf("%d", &id), id > 0)
	{
		scanf("%f", &alt);
		if(id >= 18 && alt > 1.60)
		{
			count++;		
		}	
	}
	printf("Quantidade de pessoas com mais de 1.60: %d/n", count);
	return 0;
}